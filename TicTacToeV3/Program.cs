﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GameCore;

namespace TicTacToeV3
{
    class Program
    {
        static void Main(string[] args)
        {
            Game game = new Game();

            game.Start();

            Console.WriteLine("Thanks for playing");
            Console.ReadLine();
        }
    }
}
