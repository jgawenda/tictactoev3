﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GameCore
{
    class Helper
    {
        private char[] _alphabet = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i' };

        public char GetCharFromAlphabet(int number)
        {
            if (number <= _alphabet.Length)
                return _alphabet[number-1];
            else
                return _alphabet[_alphabet.Length-1];
        }

        public int TranslateCharToNumber(char userInput)
        {
            bool exists = false;
            foreach (char character in _alphabet)
            {
                if (character == userInput)
                {
                    exists = true;
                    break;
                }
            }

            if (exists)
                return Array.IndexOf(_alphabet, userInput);
            else return -1;
        }

        public bool AreCoordsOk(string userInput, int boardSize)
        {
            if (userInput.Length == 2)
            {
                //check if 1st is char
                if (!Char.TryParse(userInput.Substring(0, 1), out char tempChar))
                    return false;

                //check if 1st is in the alphabet
                int coordX = TranslateCharToNumber(tempChar) + 1;
                if (coordX < 1 || coordX > boardSize)
                    return false;

                //check if 2nd is int
                if (!Int32.TryParse(userInput.Substring(1, 1), out int coordY))
                    return false;

                //check if 2nd is in a range (min max)
                if (coordY < 1 || coordY > boardSize)
                    return false;

                //Console.WriteLine("Are coords ok?: {0} {1}", coordX, coordY);

                return true;
            }
            else
                return false;
        }

        public int[] GetCoordsFromString(string userInput)
        {
            int[] properCoords = new int[] { 0, 0 };

            try
            {
                char tempChar = Char.Parse(userInput.Substring(0, 1));
                int coordX = TranslateCharToNumber(tempChar) + 1;
                properCoords[0] = coordX;
                int coordY = Int32.Parse(userInput.Substring(1, 1));
                properCoords[1] = coordY;
            }
            catch (Exception ex)
            {
                Console.WriteLine("Error occured when processing the coordinates: {0}", ex.Message);
                properCoords[0] = -1;
                properCoords[1] = -1;
            }

            return properCoords;
        }
    }
}
