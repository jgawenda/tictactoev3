﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Boards;
using ArtificialIntelligence;

namespace GameCore
{
    public class Game
    {
        private Board _gameBoard;
        private Helper _helper = new Helper();
        private AI _ai = new AI();
        private int _gameMode = 0;

        private char _player1Mark = 'X';
        private char _player2Mark = 'O';

        public Game()
        {
            Console.WriteLine("Welcome to Tic Tac Toe Game.\nPress Enter to continue...\n");
            Console.ReadLine();

            string userInputBoardSize = "";
            int boardSize = 3;
            do
            {
                Console.Write("Enter the board size (3-9) or leave empty and press enter to set default: ");
                userInputBoardSize = Console.ReadLine();

                if (userInputBoardSize == "")
                    break;

            } while (!Int32.TryParse(userInputBoardSize, out boardSize) || boardSize > 9 || boardSize < 3);
            
            Console.Clear();
            Console.WriteLine("Board size is now set to {0}", boardSize);

            _gameBoard = new Board(boardSize);

            Console.WriteLine("Press enter to continue...");
            Console.ReadLine();
            Console.Clear();
        }

        public void Start()
        {
            SelectMode();
            
            switch (_gameMode)
            {
                case 1:
                    PlayerVsPlayer();
                    break;
                case 2:
                    PlayerVsAI();
                    break;
            }
        }

        private void SelectMode()
        {
            Console.WriteLine("Select mode:");
            Console.WriteLine("1. Player vs Player");
            Console.WriteLine("2. Player vs AI");
            Console.WriteLine("------------\n");

            string userInputMode = "";
            int modeSelect = 0;
            do
            {
                Console.Write("Enter number: ");
                userInputMode = Console.ReadLine();
                Int32.TryParse(userInputMode, out modeSelect);

            } while (modeSelect < 1 || modeSelect > 2);

            _gameMode = modeSelect;
        }

        private int GetIntFromUser()
        {
            int output = 0;
            string userInput = "";

            do
            {
                userInput = Console.ReadLine();
            } while (Int32.TryParse(userInput, out output));

            return output;
        }

        private void PlayerVsPlayer()
        {
            do
            {
                Console.Clear();
                PlayerTurn(_player1Mark);
                if (_gameBoard.IsGameOver(_player1Mark))
                    break;

                Console.Clear();
                PlayerTurn(_player2Mark);
                if (_gameBoard.IsGameOver(_player2Mark))
                    break;

            } while (true);
        }

        private bool RollTheDice()
        {
            Console.Clear();
            Console.WriteLine("Ok, let's see who's gonna be first...");
            Console.Write("Press enter to roll the dice.");
            Console.ReadLine();

            Random random = new Random();
            int computerRoll = 0;
            int playerRoll = 0;
            for (int i = 0; i < 500; i++)
            {
                computerRoll = random.Next(0, 10);
            }

            Console.WriteLine("\n-----------");
            Console.WriteLine("Computer rolled: {0}", computerRoll);

            do
            {
                for (int i = 0; i < 500; i++)
                {
                    playerRoll = random.Next(0, 10);
                }
            } while (playerRoll == computerRoll);

            Console.WriteLine("Player roll: {0}", playerRoll);

            //true if user is player 1, else false
            if (playerRoll > computerRoll)
            {
                Console.WriteLine("Player first");
                Console.ReadLine();
                return true;
            }
            else
            {
                Console.WriteLine("Computer first");
                Console.ReadLine();
                return false;
            }
        }

        private void PlayerVsAI()
        {
            //roll the dice first to see who is going to be player1
            if (RollTheDice())
            {
                do
                {
                    PlayerTurn(_player1Mark);
                    if (_gameBoard.IsGameOver(_player1Mark))
                        break;

                    AITurn(_player2Mark, _player1Mark);
                    if (_gameBoard.IsGameOver(_player2Mark))
                        break;

                } while (true);
            }
            else
            {
                do
                {
                    AITurn(_player1Mark, _player2Mark);
                    if (_gameBoard.IsGameOver(_player1Mark))
                        break;

                    PlayerTurn(_player2Mark);
                    if (_gameBoard.IsGameOver(_player2Mark))
                        break;
                    
                } while (true);
            }

            Console.ReadLine();

        }
        
        private void PlayerTurn(char playerMark)
        {
            int[] coordsIntArray;
            //keep repeating shots until shot is completed
            do
            {
                Console.Clear();
                Console.WriteLine("Player {0} turn", playerMark);
                _gameBoard.Display();

                Console.WriteLine("\n-------------\nEnter the coordinates to place your mark");

                string userInput = " ";

                //get the coords from user until they are OK
                do
                {
                    Console.Write("(a-{0})(1-{1}): ", _helper.GetCharFromAlphabet(_gameBoard.BoardSize), _gameBoard.BoardSize);
                    userInput = Console.ReadLine();
                } while (!_helper.AreCoordsOk(userInput, _gameBoard.BoardSize));

                //Console.WriteLine("Coords are: {0}", _helper.AreCoordsOk(userInput, _gameBoard.BoardSize));
                //Console.ReadLine();

                coordsIntArray = _helper.GetCoordsFromString(userInput);

            } while (!_gameBoard.Shot(playerMark, coordsIntArray[0], coordsIntArray[1], true));

            Console.Clear();
            _gameBoard.Display();
        }

        private void AITurn(char playerMark, char enemyMark)
        {
            _ai.Shot(_gameBoard, playerMark, enemyMark);

            Console.Clear();
            _gameBoard.Display();
        }

    }
}
