﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Boards
{
    public class Board
    {
        public char[,] ShotsCoords { get; private set; }

        public int BoardSize { get; private set; }
        
        private char[] _alphabet = new char[] { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i' };
        
        public Board(int size)
        {
            BoardSize = size;

            if (BoardSize > _alphabet.Length)
            {
                Console.WriteLine("Size is too big. Resizing to: {0}", _alphabet.Length);
                BoardSize = _alphabet.Length;
                Console.ReadLine();
            }

            ShotsCoords = new char[BoardSize, BoardSize];

            //zeroing the board
            for (int i = 0; i < BoardSize; i++)
            {
                for (int j = 0; j < BoardSize; j++)
                {
                    ShotsCoords[i, j] = ' ';
                }
            }
            
        }

        public void Display()
        {
            Console.Write("  |");
            
            for (int i=0; i<BoardSize; i++)
            {
                Console.Write(" {0} |", _alphabet[i]);
            }

            for (int i = 0; i < BoardSize; i++)
            {
                //display rows
                ShowSeparator();
                Console.Write(" {0}|", i + 1);

                for (int j=0; j < BoardSize; j++)
                {
                    //display columns
                    Console.Write(" {0} |", ShotsCoords[i,j]);
                }

            }

            ShowSeparator();
        }
        
        private void ShowSeparator()
        {
            Console.WriteLine("");
            Console.ForegroundColor = ConsoleColor.DarkGray;
            Console.Write("---");

            for (int i=0; i<BoardSize; i++)
            {
                Console.Write("----");
            }

            Console.ForegroundColor = ConsoleColor.Gray;
            Console.WriteLine("");
        }

        public bool Shot(char userChar, int x, int y, bool showErrors)
        {
            x--;
            y--;

            //Console.WriteLine("SHOT : x: {0}, y: {1}", x, y);
            //Console.ReadLine();
            if (x < BoardSize && x >= 0 && y < BoardSize && y >= 0)
            {
                if (ShotsCoords[y, x] == ' ')
                {
                    ShotsCoords[y, x] = userChar;
                    return true;
                }
                else
                {
                    if (showErrors)
                    {
                        Console.WriteLine("There is another character already!");
                        Console.ReadLine();
                    }
                    return false;
                }
            }
            else
            {
                if (showErrors)
                {
                    Console.WriteLine("Out of bounds!");
                    Console.ReadLine();
                }
                return false;
            }
        }

        private bool IsGameOverByWin(char userChar)
        {
            int counter = 0;

            //check horizontally
            for (int y=0; y<BoardSize; y++)
            {
                for (int x = 0; x < BoardSize; x++)
                {
                    if (ShotsCoords[y, x] == userChar)
                        counter++;
                    else
                        counter = 0;

                    if (counter == 3)
                        return true;
                }

                counter = 0;
            }

            //check vertically
            for (int x = 0; x < BoardSize; x++)
            {
                for (int y = 0; y < BoardSize; y++)
                {
                    if (ShotsCoords[y, x] == userChar)
                        counter++;
                    else
                        counter = 0;

                    if (counter == 3)
                        return true;
                }

                counter = 0;
            }

            //check "\" (left-down corner)
            int howManyCols = BoardSize;
            int diff = 0;
            for (int y=0; y<BoardSize-2; y++)
            {
                for (int x=0; x<howManyCols; x++)
                {
                    if (ShotsCoords[x + diff, x] == userChar)
                        counter++;
                    else
                        counter = 0;

                    if (counter == 3)
                        return true;

                    //ShotsCoords[x + diff, x] = 'O';
                }

                counter = 0;
                howManyCols--;
                diff++;
            }

            //check "\" (right-top corner)
            howManyCols = BoardSize - 1;
            diff = 1;
            for (int x = 0; x < BoardSize - 3; x++)
            {
                for (int y = 0; y < howManyCols; y++)
                {
                    if (ShotsCoords[y, y + diff] == userChar)
                        counter++;
                    else
                        counter = 0;

                    if (counter == 3)
                        return true;

                    //ShotsCoords[y, y + diff] = 'O';
                }

                counter = 0;
                howManyCols--;
                diff++;
            }

            //check "/" (left-top corner)
            howManyCols = BoardSize;
            diff = BoardSize - 1;
            for (int y = 0; y < BoardSize - 2; y++)
            {
                for (int x = 0; x < howManyCols; x++)
                {
                    //Console.WriteLine("{0} {1}", diff, x);
                    //Console.ReadLine();
                    //ShotsCoords[diff, x] = 'O';

                    if (ShotsCoords[diff, x] == userChar)
                    {
                        counter++;
                    }
                    else
                        counter = 0;

                    if (counter == 3)
                        return true;

                    diff--;
                }

                counter = 0;
                diff = BoardSize - y - 2;
                howManyCols--;
            }

            //check "/" (left-down corner)
            howManyCols = BoardSize - 1;
            diff = BoardSize - 1;
            int yHelper = 1;
            for (int x = 0; x < BoardSize - 3; x++)
            {
                yHelper = x + 1;
                for (int y = 0; y < howManyCols; y++)
                {
                    /*
                    Console.WriteLine("{0} {1}", diff, yHelper);
                    
                    ShotsCoords[diff, yHelper] = 'O';
                    Display();
                    Console.ReadLine();
                    Console.Clear();
                    */
                    
                    if (ShotsCoords[diff, yHelper] == userChar)
                    {
                        counter++;
                    }
                    else
                        counter = 0;

                    if (counter == 3)
                        return true;

                    diff--;
                    yHelper++;

                }
                
                counter = 0;
                diff = BoardSize - 1;
                howManyCols--;
            }
            
            return false;
        }

        private bool IsGameOverByNoShotsLeft()
        {
            bool areThereAnyEmptySpaces = false;
            foreach (char shot in ShotsCoords)
            {
                if (shot == ' ')
                {
                    areThereAnyEmptySpaces = true;
                    break;
                }
            }
            if (!areThereAnyEmptySpaces)
                return true;

            return false;
        }

        public bool IsGameOver(char userChar)
        {
            //check if game is over because player win
            if (IsGameOverByWin(userChar))
            {
                Console.BackgroundColor = ConsoleColor.Green;
                Console.ForegroundColor = ConsoleColor.Black;
                Console.WriteLine("Player {0} WINS! Gongratulations", userChar);
                Console.ForegroundColor = ConsoleColor.Gray;
                Console.BackgroundColor = ConsoleColor.Black;
                return true;
            }

            //if not, check if board went out of shots
            if (IsGameOverByNoShotsLeft())
            {
                Console.WriteLine("There are no more space available. Game is over and it's a TIE. GG");
                return true;
            }

            return false;
        }
    }
}
