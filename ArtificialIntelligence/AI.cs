﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Boards;

namespace ArtificialIntelligence
{
    public class AI
    {
        private Random _myRandom = new Random();

        private bool _amIFirstPlayer = false;

        private int _shotsCounter = 0;

        private int[] _myLastShot = new int[] { -1, -1 };

        private void SetMyLastShotTo(int y, int x)
        {
            _myLastShot[0] = y;
            _myLastShot[1] = x;
        }

        private void CheckIfIAmFirstPlayer(Board board)
        {
            bool areThereAnyShotsAlready = false;
            foreach (char cell in board.ShotsCoords)
            {
                if (cell != ' ')
                {
                    areThereAnyShotsAlready = true;
                    break;
                }
            }

            if (areThereAnyShotsAlready)
                _amIFirstPlayer = false;
            else
                _amIFirstPlayer = true;
        }

        private bool ScanForFirstEnemyShotAndHit(Board board, char aiMark, char enemyMark)
        {
            for (int y=0; y<board.BoardSize; y++)
            {
                for (int x=0; x<board.BoardSize; x++)
                {
                    if (board.ShotsCoords[y, x] == enemyMark)
                    {
                        //Console.WriteLine("first enemy shot found");
                        //shoot top left if possible
                        if (x - 1 >= 0 && y - 1 >= 0 && board.ShotsCoords[y - 1, x - 1] == ' ')
                        {
                            SetMyLastShotTo(y - 1, x - 1);
                            return board.Shot(aiMark, x, y, true);
                        }
                        //shoot down right if possible
                        else if (x + 1 < board.BoardSize && y + 1 < board.BoardSize && board.ShotsCoords[y + 1, x + 1] == ' ')
                        {
                            SetMyLastShotTo(y + 1, x + 1);
                            return board.Shot(aiMark, x + 2, y + 2, true);
                        }
                        //shoot top right if possible
                        else if (x + 1 < board.BoardSize && y - 1 >= 0 && board.ShotsCoords[y - 1, x + 1] == ' ')
                        {
                            SetMyLastShotTo(y - 1, x + 1);
                            return board.Shot(aiMark, x + 2, y, true);
                        }
                        //shoot down left if possible
                        else if (x - 1 >= 0 && y + 1 < board.BoardSize && board.ShotsCoords[y + 1, x - 1] == ' ')
                        {
                            SetMyLastShotTo(y + 1, x - 1);
                            return board.Shot(aiMark, x, y + 2, true);
                        }
                        //shoot to the left if possible
                        else if (x - 1 >= 0 && board.ShotsCoords[y, x - 1] == ' ')
                        {
                            //Console.WriteLine("shot to the left");
                            SetMyLastShotTo(y, x - 1);
                            return board.Shot(aiMark, x, y + 1, true);
                        }
                        //shoot to the right if possible
                        else if (x + 1 < board.BoardSize && board.ShotsCoords[y, x + 1] == ' ')
                        {
                            //Console.WriteLine("shot to the right");
                            SetMyLastShotTo(y, x + 1);
                            return board.Shot(aiMark, x + 2, y + 1, true);
                        }
                        //shoot above if possible
                        else if (y - 1 >= 0 && board.ShotsCoords[y - 1, x] == ' ')
                        {
                            //Console.WriteLine("shot above");
                            SetMyLastShotTo(y - 1, x);
                            return board.Shot(aiMark, x + 1, y, true);
                        }
                        //shoot under if possible
                        else if (y + 1 < board.BoardSize && board.ShotsCoords[y + 1, x] == ' ')
                        {
                            //Console.WriteLine("shot under");
                            SetMyLastShotTo(y + 1, x);
                            return board.Shot(aiMark, x + 1, y + 2, true);
                        }
                    }
                }
            }

            return false;
        }

        private bool CheckEnemies2InARowAndShootThem(Board board, char aiMark, char enemyMark)
        {
            int counter = 0;

            //check horizontally if there are 2 enemy marks in one row and if yes, shoot near them
            for (int y = 0; y < board.BoardSize; y++)
            {
                for (int x = 0; x < board.BoardSize; x++)
                {
                    if (x+2 < board.BoardSize)
                    {
                        for (int i=0; i<3; i++)
                        {
                            if (board.ShotsCoords[y, x + i] == enemyMark)
                            {
                                //Console.WriteLine("counter ++ {0} {1}", x+i, y);
                                counter++;
                            }
                            else if (board.ShotsCoords[y, x + i] == aiMark)
                            {
                                //Console.WriteLine("counter 0");
                                counter = 0;
                            }
                        }
                        //if we find 2 out of 3 as enemy, then shoot between or at the beginning or at the end
                        if (counter == 2 && board.ShotsCoords[y, x] != aiMark)
                        {
                            //Console.WriteLine("Found 2 in one row! {0} {1}", x + 1, y + 1);
                            //Console.ReadLine();
                            for (int i = 0; i < 3; i++)
                            {
                                if (board.ShotsCoords[y, x + i] == ' ')
                                {
                                    SetMyLastShotTo(y, x + i);
                                    return board.Shot(aiMark, x + 1 + i, y + 1, false);
                                }
                            }
                        }
                    }

                    counter = 0;

                }

                counter = 0;
            }
            
            //check vertically if there are 2 enemy marks in one row and if yes, shoot near them
            for (int x = 0; x < board.BoardSize; x++)
            {
                for (int y = 0; y < board.BoardSize; y++)
                {
                    if (y + 2 < board.BoardSize)
                    {
                        for (int i = 0; i < 3; i++)
                        {
                            if (board.ShotsCoords[y + i, x] == enemyMark)
                            {
                                //Console.WriteLine("counter ++ {0} {1}", x+i, y);
                                counter++;
                            }
                            else if (board.ShotsCoords[y + i, x] == aiMark)
                            {
                                //Console.WriteLine("counter 0");
                                counter = 0;
                            }
                        }
                        //if we find 2 out of 3 as enemy, then shoot between or at the beginning or at the end
                        if (counter == 2 && board.ShotsCoords[y, x] != aiMark)
                        {
                            //Console.WriteLine("Found 2 in one row! {0} {1}", x + 1, y + 1);
                            //Console.ReadLine();
                            for (int i = 0; i < 3; i++)
                            {
                                if (board.ShotsCoords[y + i, x] == ' ')
                                {
                                    SetMyLastShotTo(y + i, x);
                                    return board.Shot(aiMark, x + 1, y + 1 + i, false);
                                }
                            }
                        }
                    }

                    counter = 0;
                }

                counter = 0;
            }
            
            //check "\" (left-down corner)
            int howManyCols = board.BoardSize;
            int diff = 0;
            for (int y = 0; y < board.BoardSize - 2; y++)
            {
                for (int x = 0; x < howManyCols; x++)
                {
                    if (x + 2 < board.BoardSize && x + 2 + diff < board.BoardSize)
                    {
                        for (int i = 0; i < 3; i++)
                        {
                            //Console.Clear();
                            //Console.WriteLine("Checking: {0} {1}", x + diff + i, x + i);
                            //board.Shot('H', x + i + 1, x + diff + i + 1, false);
                            //board.Display();
                            //Console.ReadLine();
                            if (board.ShotsCoords[x + diff + i, x + i] == enemyMark)
                                counter++;
                            else if (board.ShotsCoords[x + diff + i, x + i] == aiMark)
                                counter = 0;
                        }

                        //if we find 2 out of 3 as enemy, then shoot between or at the beginning or at the end
                        if (counter == 2 && board.ShotsCoords[x + diff, x] != aiMark)
                        {
                            //Console.WriteLine("Found 2 in one row! {0} {1}", x + diff + 1, x + 1);
                            //Console.ReadLine();
                            for (int i = 0; i < 3; i++)
                            {
                                if (board.ShotsCoords[x + diff + i, x + i] == ' ')
                                {
                                    SetMyLastShotTo(x + diff + i, x + i);
                                    return board.Shot(aiMark, x + i + 1, x + diff + i + 1, true);
                                }
                            }
                        }
                    }

                    counter = 0;

                }

                counter = 0;
                howManyCols--;
                diff++;
            }
            

            //check "\" (right-top corner)
            howManyCols = board.BoardSize - 1;
            diff = 1;
            for (int x = 0; x < board.BoardSize - 3; x++)
            {
                for (int y = 0; y < howManyCols; y++)
                {
                    if (y + 2 < board.BoardSize && y + 2 + diff < board.BoardSize)
                    {
                        for (int i = 0; i < 3; i++)
                        {
                            //Console.Clear();
                            //Console.WriteLine("Checking: {0} {1}", y + i, y + diff + i);
                            //board.Shot('H', y + diff + i + 1, y + i + 1, false);
                            //board.Display();
                            //Console.ReadLine();
                            if (board.ShotsCoords[y + i, y + diff + i] == enemyMark)
                                counter++;
                            else if (board.ShotsCoords[y + i, y + diff + i] == aiMark)
                                counter = 0;
                        }

                        //if we find 2 out of 3 as enemy, then shoot between or at the beginning or at the end
                        if (counter == 2 && board.ShotsCoords[y, y + diff] != aiMark)
                        {
                            //Console.WriteLine("Found 2 in one row! {0} {1}", x + diff + 1, x + 1);
                            //Console.ReadLine();
                            for (int i = 0; i < 3; i++)
                            {
                                if (board.ShotsCoords[y + i, y + diff + i] == ' ')
                                {
                                    //Console.WriteLine("Shot: {0} {1}", y + diff + i + 1, y + i + 1);
                                    SetMyLastShotTo(y + i, y + diff + i);
                                    return board.Shot(aiMark, y + diff + i + 1, y + i + 1, true);
                                }
                            }
                        }
                    }

                    counter = 0;

                }

                counter = 0;
                howManyCols--;
                diff++;
            }
            

            //check "/" (left-top corner)
            howManyCols = board.BoardSize;
            diff = board.BoardSize - 1;
            for (int y = 0; y < board.BoardSize - 2; y++)
            {
                for (int x = 0; x < howManyCols; x++)
                {
                    if (x + 2 < board.BoardSize && diff - 2 >= 0)
                    {
                        for (int i = 0; i < 3; i++)
                        {
                            //Console.Clear();
                            //Console.WriteLine("Checking: {0} {1}", diff - i, x + i);
                            //board.Shot('H', x + i + 1, diff - i + 1, false);
                            //board.Display();
                            //Console.ReadLine();
                            if (board.ShotsCoords[diff - i, x + i] == enemyMark)
                                counter++;
                            else if (board.ShotsCoords[diff - i, x + i] == aiMark)
                                counter = 0;
                        }

                        //if we find 2 out of 3 as enemy, then shoot between or at the beginning or at the end
                        if (counter == 2 && board.ShotsCoords[diff, x] != aiMark)
                        {
                            //Console.WriteLine("Found 2 in one row! {0} {1}", diff, x);
                            //Console.ReadLine();
                            for (int i = 0; i < 3; i++)
                            {
                                if (board.ShotsCoords[diff - i, x + i] == ' ')
                                {
                                    SetMyLastShotTo(diff - i, x + i);
                                    return board.Shot(aiMark, x + i + 1, diff - i + 1, true);
                                }
                            }
                        }
                    }

                    counter = 0;
                    diff--;
                }

                counter = 0;
                diff = board.BoardSize - y - 2;
                howManyCols--;
            }
            

            //check "/" (left-down corner)
            howManyCols = board.BoardSize - 1;
            diff = board.BoardSize - 1;
            int yHelper = 1;
            for (int x = 0; x < board.BoardSize - 3; x++)
            {
                yHelper = x + 1;
                for (int y = 0; y < howManyCols; y++)
                {
                    if (yHelper + 2 < board.BoardSize && diff - 2 >= 0)
                    {
                        for (int i = 0; i < 3; i++)
                        {
                            if (board.ShotsCoords[diff - i, yHelper + i] == enemyMark)
                                counter++;
                            else if (board.ShotsCoords[diff - i, yHelper + i] == aiMark)
                                counter = 0;
                        }

                        //if we find 2 out of 3 as enemy, then shoot between or at the beginning or at the end
                        if (counter == 2 && board.ShotsCoords[diff, yHelper] != aiMark)
                        {
                            //Console.WriteLine("Found 2 in one row! {0} {1}", diff, x);
                            //Console.ReadLine();
                            for (int i = 0; i < 3; i++)
                            {
                                if (board.ShotsCoords[diff - i, yHelper + i] == ' ')
                                {
                                    SetMyLastShotTo(diff - i, yHelper + i);
                                    return board.Shot(aiMark, yHelper + i + 1, diff - i + 1, true);
                                }
                            }
                        }
                    }
                    
                    counter = 0;
                    diff--;
                    yHelper++;

                }

                counter = 0;
                diff = board.BoardSize - 1;
                howManyCols--;
            }
            

            return false;
        }

        private bool CheckMy2InARowAndShootThem(Board board, char aiMark, char enemyMark)
        {
            int counter = 0;

            //check horizontally if there are 2 my marks in one row and if yes, shoot near them
            for (int y = 0; y < board.BoardSize; y++)
            {
                for (int x = 0; x < board.BoardSize; x++)
                {
                    if (x + 2 < board.BoardSize)
                    {
                        for (int i = 0; i < 3; i++)
                        {
                            if (board.ShotsCoords[y, x + i] == aiMark)
                            {
                                //Console.WriteLine("counter ++ {0} {1}", x+i, y);
                                counter++;
                            }
                            else if (board.ShotsCoords[y, x + i] == enemyMark)
                            {
                                //Console.WriteLine("counter 0");
                                counter = 0;
                            }
                        }
                        //if we find 2 out of 3 as mine, then shoot between or at the beginning or at the end
                        if (counter == 2 && board.ShotsCoords[y, x] != enemyMark)
                        {
                            //Console.WriteLine("Found 2 in one row! {0} {1}", x + 1, y + 1);
                            //Console.ReadLine();
                            for (int i = 0; i < 3; i++)
                            {
                                if (board.ShotsCoords[y, x + i] == ' ')
                                {
                                    SetMyLastShotTo(y, x + i);
                                    return board.Shot(aiMark, x + 1 + i, y + 1, false);
                                }
                            }
                        }
                    }

                    counter = 0;
                }

                counter = 0;
            }

            //check vertically if there are 2 my marks in one row and if yes, shoot near them
            for (int x = 0; x < board.BoardSize; x++)
            {
                for (int y = 0; y < board.BoardSize; y++)
                {
                    if (y + 2 < board.BoardSize)
                    {
                        for (int i = 0; i < 3; i++)
                        {
                            if (board.ShotsCoords[y + i, x] == aiMark)
                            {
                                //Console.WriteLine("counter ++ {0} {1}", x+i, y);
                                counter++;
                            }
                            else if (board.ShotsCoords[y + i, x] == enemyMark)
                            {
                                //Console.WriteLine("counter 0");
                                counter = 0;
                            }
                        }
                        //if we find 2 out of 3 as mine, then shoot between or at the beginning or at the end
                        if (counter == 2 && board.ShotsCoords[y, x] != enemyMark)
                        {
                            //Console.WriteLine("Found 2 in one row! {0} {1}", x + 1, y + 1);
                            //Console.ReadLine();
                            for (int i = 0; i < 3; i++)
                            {
                                if (board.ShotsCoords[y + i, x] == ' ')
                                {
                                    return board.Shot(aiMark, x + 1, y + 1 + i, false);
                                }
                            }
                        }
                    }

                    counter = 0;
                }

                counter = 0;
            }

            //check "\" (left-down corner)
            int howManyCols = board.BoardSize;
            int diff = 0;
            for (int y = 0; y < board.BoardSize - 2; y++)
            {
                for (int x = 0; x < howManyCols; x++)
                {
                    if (x + 2 < board.BoardSize && x + 2 + diff < board.BoardSize)
                    {
                        for (int i = 0; i < 3; i++)
                        {
                            //Console.Clear();
                            //Console.WriteLine("Checking: {0} {1}", x + diff + i, x + i);
                            //board.Shot('H', x + i + 1, x + diff + i + 1, false);
                            //board.Display();
                            //Console.ReadLine();
                            if (board.ShotsCoords[x + diff + i, x + i] == aiMark)
                                counter++;
                            else if (board.ShotsCoords[x + diff + i, x + i] == enemyMark)
                                counter = 0;
                        }

                        //if we find 2 out of 3 as enemy, then shoot between or at the beginning or at the end
                        if (counter == 2 && board.ShotsCoords[x + diff, x] != enemyMark)
                        {
                            //Console.WriteLine("Found 2 in one row! {0} {1}", x + diff + 1, x + 1);
                            //Console.ReadLine();
                            for (int i = 0; i < 3; i++)
                            {
                                if (board.ShotsCoords[x + diff + i, x + i] == ' ')
                                {
                                    SetMyLastShotTo(x + diff + i, x + i);
                                    return board.Shot(aiMark, x + i + 1, x + diff + i + 1, true);
                                }
                            }
                        }
                    }

                    counter = 0;

                }

                counter = 0;
                howManyCols--;
                diff++;
            }


            //check "\" (right-top corner)
            howManyCols = board.BoardSize - 1;
            diff = 1;
            for (int x = 0; x < board.BoardSize - 3; x++)
            {
                for (int y = 0; y < howManyCols; y++)
                {
                    if (y + 2 < board.BoardSize && y + 2 + diff < board.BoardSize)
                    {
                        for (int i = 0; i < 3; i++)
                        {
                            //Console.Clear();
                            //Console.WriteLine("Checking: {0} {1}", y + i, y + diff + i);
                            //board.Shot('H', y + diff + i + 1, y + i + 1, false);
                            //board.Display();
                            //Console.ReadLine();
                            if (board.ShotsCoords[y + i, y + diff + i] == aiMark)
                                counter++;
                            else if (board.ShotsCoords[y + i, y + diff + i] == enemyMark)
                                counter = 0;
                        }

                        //if we find 2 out of 3 as enemy, then shoot between or at the beginning or at the end
                        if (counter == 2 && board.ShotsCoords[y, y + diff] != enemyMark)
                        {
                            //Console.WriteLine("Found 2 in one row! {0} {1}", x + diff + 1, x + 1);
                            //Console.ReadLine();
                            for (int i = 0; i < 3; i++)
                            {
                                if (board.ShotsCoords[y + i, y + diff + i] == ' ')
                                {
                                    //Console.WriteLine("Shot: {0} {1}", y + diff + i + 1, y + i + 1);
                                    SetMyLastShotTo(y + i, y + diff + i);
                                    return board.Shot(aiMark, y + diff + i + 1, y + i + 1, true);
                                }
                            }
                        }
                    }

                    counter = 0;

                }

                counter = 0;
                howManyCols--;
                diff++;
            }


            //check "/" (left-top corner)
            howManyCols = board.BoardSize;
            diff = board.BoardSize - 1;
            for (int y = 0; y < board.BoardSize - 2; y++)
            {
                for (int x = 0; x < howManyCols; x++)
                {
                    if (x + 2 < board.BoardSize && diff - 2 >= 0)
                    {
                        for (int i = 0; i < 3; i++)
                        {
                            //Console.Clear();
                            //Console.WriteLine("Checking: {0} {1}", diff - i, x + i);
                            //board.Shot('H', x + i + 1, diff - i + 1, false);
                            //board.Display();
                            //Console.ReadLine();
                            if (board.ShotsCoords[diff - i, x + i] == aiMark)
                                counter++;
                            else if (board.ShotsCoords[diff - i, x + i] == enemyMark)
                                counter = 0;
                        }

                        //if we find 2 out of 3 as enemy, then shoot between or at the beginning or at the end
                        if (counter == 2 && board.ShotsCoords[diff, x] != enemyMark)
                        {
                            //Console.WriteLine("Found 2 in one row! {0} {1}", diff, x);
                            //Console.ReadLine();
                            for (int i = 0; i < 3; i++)
                            {
                                if (board.ShotsCoords[diff - i, x + i] == ' ')
                                {
                                    SetMyLastShotTo(diff - i, x + i);
                                    return board.Shot(aiMark, x + i + 1, diff - i + 1, true);
                                }
                            }
                        }
                    }

                    counter = 0;
                    diff--;
                }

                counter = 0;
                diff = board.BoardSize - y - 2;
                howManyCols--;
            }


            //check "/" (left-down corner)
            howManyCols = board.BoardSize - 1;
            diff = board.BoardSize - 1;
            int yHelper = 1;
            for (int x = 0; x < board.BoardSize - 3; x++)
            {
                yHelper = x + 1;
                for (int y = 0; y < howManyCols; y++)
                {
                    if (yHelper + 2 < board.BoardSize && diff - 2 >= 0)
                    {
                        for (int i = 0; i < 3; i++)
                        {
                            if (board.ShotsCoords[diff - i, yHelper + i] == aiMark)
                                counter++;
                            else if (board.ShotsCoords[diff - i, yHelper + i] == enemyMark)
                                counter = 0;
                        }

                        //if we find 2 out of 3 as enemy, then shoot between or at the beginning or at the end
                        if (counter == 2 && board.ShotsCoords[diff, yHelper] != enemyMark)
                        {
                            //Console.WriteLine("Found 2 in one row! {0} {1}", diff, x);
                            //Console.ReadLine();
                            for (int i = 0; i < 3; i++)
                            {
                                if (board.ShotsCoords[diff - i, yHelper + i] == ' ')
                                {
                                    SetMyLastShotTo(diff - i, yHelper + i);
                                    return board.Shot(aiMark, yHelper + i + 1, diff - i + 1, true);
                                }
                            }
                        }
                    }

                    counter = 0;
                    diff--;
                    yHelper++;

                }

                counter = 0;
                diff = board.BoardSize - 1;
                howManyCols--;
            }


            return false;
        }

        private bool ShootNearMyLastHit(Board board, char aiMark)
        {
            if (_myLastShot[0] >= 0 && +_myLastShot[1] >= 0)
            {
                //Console.WriteLine("Shoot near my last hit");
                //shoot top left if possible
                if (_myLastShot[1] - 1 >= 0 && _myLastShot[0] - 1 >= 0 && board.ShotsCoords[_myLastShot[0] - 1, _myLastShot[1] - 1] == ' ' && _myLastShot[1] - 2 >= 0 && _myLastShot[0] - 2 >= 0 && board.ShotsCoords[_myLastShot[0] - 2, _myLastShot[1] - 2] == ' ')
                {
                    SetMyLastShotTo(_myLastShot[0] - 1, _myLastShot[1] - 1);
                    return board.Shot(aiMark, _myLastShot[1] + 1, _myLastShot[0] + 1, true);
                }
                //shoot down right if possible
                else if (_myLastShot[1] + 1 < board.BoardSize && _myLastShot[0] + 1 < board.BoardSize && board.ShotsCoords[_myLastShot[0] + 1, _myLastShot[1] + 1] == ' ' && _myLastShot[1] + 2 < board.BoardSize && _myLastShot[0] + 2 < board.BoardSize && board.ShotsCoords[_myLastShot[0] + 2, _myLastShot[1] + 2] == ' ')
                {
                    SetMyLastShotTo(_myLastShot[0] + 1, _myLastShot[1] + 1);
                    return board.Shot(aiMark, _myLastShot[1] + 1, _myLastShot[0] + 1, true);
                }
                //shoot top right if possible
                else if (_myLastShot[1] + 1 < board.BoardSize && _myLastShot[0] - 1 >= 0 && board.ShotsCoords[_myLastShot[0] - 1, _myLastShot[1] + 1] == ' ' && _myLastShot[1] + 2 < board.BoardSize && _myLastShot[0] - 2 >= 0 && board.ShotsCoords[_myLastShot[0] - 2, _myLastShot[1] + 2] == ' ')
                {
                    SetMyLastShotTo(_myLastShot[0] - 1, _myLastShot[1] + 1);
                    return board.Shot(aiMark, _myLastShot[1] + 1, _myLastShot[0] + 1, true);
                }
                //shoot down left if possible
                else if (_myLastShot[1] - 1 >= 0 && _myLastShot[0] + 1 < board.BoardSize && board.ShotsCoords[_myLastShot[0] + 1, _myLastShot[1] - 1] == ' ' && _myLastShot[1] - 2 >= 0 && _myLastShot[0] + 2 < board.BoardSize && board.ShotsCoords[_myLastShot[0] + 2, _myLastShot[1] - 2] == ' ')
                {
                    SetMyLastShotTo(_myLastShot[0] + 1, _myLastShot[1] - 1);
                    return board.Shot(aiMark, _myLastShot[1] + 1, _myLastShot[0] + 1, true);
                }
                //shoot to the left if possible
                else if (_myLastShot[1] - 1 >= 0 && board.ShotsCoords[_myLastShot[0], _myLastShot[1] - 1] == ' ' && _myLastShot[1] - 2 >= 0 && board.ShotsCoords[_myLastShot[0], _myLastShot[1] - 2] == ' ')
                {
                    _myLastShot[1]--;
                    return board.Shot(aiMark, _myLastShot[1] + 1, _myLastShot[0] + 1, true);
                }
                //shoot to the right if possible
                else if (_myLastShot[1] + 1 < board.BoardSize && board.ShotsCoords[_myLastShot[0], _myLastShot[1] + 1] == ' ' && _myLastShot[1] + 2 < board.BoardSize && board.ShotsCoords[_myLastShot[0], _myLastShot[1] + 2] == ' ')
                {
                    _myLastShot[1]++;
                    return board.Shot(aiMark, _myLastShot[1] + 1, _myLastShot[0] + 1, true);
                }
                //shoot above if possible
                else if (_myLastShot[0] - 1 >= 0 && board.ShotsCoords[_myLastShot[0] - 1, _myLastShot[1]] == ' ' && _myLastShot[0] - 2 >= 0 && board.ShotsCoords[_myLastShot[0] - 2, _myLastShot[1]] == ' ')
                {
                    _myLastShot[0]--;
                    return board.Shot(aiMark, _myLastShot[1] + 1, _myLastShot[0] + 1, true);
                }
                //shoot under if possible
                else if (_myLastShot[0] + 1 < board.BoardSize && board.ShotsCoords[_myLastShot[0] + 1, _myLastShot[1]] == ' ' && _myLastShot[0] + 2 < board.BoardSize && board.ShotsCoords[_myLastShot[0] + 2, _myLastShot[1]] == ' ')
                {
                    _myLastShot[0]++;
                    return board.Shot(aiMark, _myLastShot[1] + 1, _myLastShot[0] + 1, true);
                }
                
                return false;
            }
            else
                return false;
        }

        private void ShootRandomPlace(Board board, char aiMark)
        {
            int tempX = 0;
            int tempY = 0;
            do
            {
                tempX = _myRandom.Next(0, board.BoardSize);
                tempY = _myRandom.Next(0, board.BoardSize);
                //Console.WriteLine("Stepped into random, try to shoot at: {0} {1}", tempY, tempX);
                //Console.ReadLine();
            } while (!board.Shot(aiMark, tempX + 1, tempY + 1, false));

            SetMyLastShotTo(tempY, tempX);
        }

        public void Shot(Board board, char computerMark, char enemyMark)
        {
            if (_shotsCounter == 0)
                CheckIfIAmFirstPlayer(board);

            //if AI is player1 and it's his first shot, then...
            if (_amIFirstPlayer && _shotsCounter == 0)
            {
                ShootRandomPlace(board, computerMark);
            }
            else
            {
                if (!CheckMy2InARowAndShootThem(board, computerMark, enemyMark))
                {
                    if (!CheckEnemies2InARowAndShootThem(board, computerMark, enemyMark))
                    {
                        if (_shotsCounter == 0)
                        {
                            if (!ScanForFirstEnemyShotAndHit(board, computerMark, enemyMark))
                            {
                                ShootRandomPlace(board, computerMark);
                            }
                        }
                        else
                        {
                            if (!ShootNearMyLastHit(board, computerMark))
                            {
                                ShootRandomPlace(board, computerMark);
                            }
                        }
                    }
                }
            }

            _shotsCounter++;
        }
    }
}
